import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService}       from "../core/api.service";
import {urlValidator} from "../shared/url.directive";
import {isUndefined} from "util";

@Component({
    selector: 'main',
    templateUrl: './main.html',
})

export class MainComponent {
    mode = 'Observable';
    mainForm: FormGroup;
    responseSubmit: {};
    responseSubmitIsError: boolean;
    responseSubmitIsSuccess: boolean;

    constructor(private fb: FormBuilder, private apiService: ApiService) {
        this.createForm();
        this.refrashModel();
    }

    createForm(): void {
        this.mainForm = this.fb.group({
            url: ['', urlValidator()],
            type: [''],
            text: [''],
        });
    }

    refrashModel(): void {
        this.responseSubmit = {};
        this.responseSubmitIsError = false;
        this.responseSubmitIsSuccess = false;
    }

    onSubmit(): void {
        this.refrashModel();
        this.apiService.getParsing(this.mainForm.value).subscribe(
            data => {
                this.responseSubmit = data;
                if(data.validation.valid) {
                    this.responseSubmitIsSuccess = true;
                }
            },
            error => {
                this.responseSubmitIsError = true;
            }
        );
    }

    getResponseErrors(fieldName) {
        return this.getDefinedData('validation.errors.' + fieldName, this.responseSubmit);
    }

    getDefinedData(name, data = null) {
        let nameArray = name.split('.');

        for (let key in nameArray) {
            if (data === null) {
                return null;
            }
            let itemName = nameArray[key];
            if (isUndefined(data[itemName])) {
                return null;
            } else {
                data = data[itemName];
            }
        }

        return data;
    }

}
