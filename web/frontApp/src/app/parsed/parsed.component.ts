import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ApiService} from "../core/api.service";
import {isUndefined} from "util";

@Component({
    selector: 'parsed',
    templateUrl: './parsed.html',
})

export class ParsedComponent implements OnInit {
    mode = 'Observable';
    responseData;
    responseIsError: boolean;
    parsedId: number;
    private sub: any;

    constructor(private apiService: ApiService, private route: ActivatedRoute) {
        this.refrashModel();
    }

    refrashModel(): void {
        this.responseData = [];
        this.responseIsError = false;
    }

    ngOnInit(): void {
        console.log('init');
        this.sub = this.route.params.subscribe(params => {
            this.parsedId = params['parsedId'] ? +params['parsedId'] : undefined;

            if (!isUndefined(this.parsedId)) {
                this.loadParsed();
            }
        });
    }

    loadParsed(): void {
        this.refrashModel();
        this.apiService.getParsed(this.parsedId).subscribe(
            data => {
                this.responseData = data;
            },
            error => {
                this.responseIsError = true;
            }
        );
    }
}
