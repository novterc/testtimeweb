import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ApiService} from "../core/api.service";

@Component({
    selector: 'list',
    templateUrl: './list.html',
})

export class ListComponent implements OnInit {
    mode = 'Observable';
    responseList;
    responseListIsError: boolean;

    constructor(private apiService: ApiService) {
        this.refrashModel();
    }

    refrashModel(): void {
        this.responseList = [];
        this.responseListIsError = false;
    }

    ngOnInit(): void {
        this.loadList();
    }

    loadList(): void {
        this.refrashModel();
        this.apiService.getList().subscribe(
            data => {
                this.responseList = data;
            },
            error => {
                this.responseListIsError = true;
            }
        );
    }
}
