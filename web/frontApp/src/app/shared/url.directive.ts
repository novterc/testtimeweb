import {ValidatorFn, AbstractControl} from "@angular/forms";


export function urlValidator(): ValidatorFn {
    let urlRegExp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;

    return (control: AbstractControl): {[key: string]: any} => {
        let forbidden = urlRegExp.test(control.value);
        console.log(forbidden);
        return forbidden ? null : { url: true};
    };
}
