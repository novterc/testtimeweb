import { Injectable }   from '@angular/core';
import { Http, Response, RequestOptions }   from '@angular/http';
import { Observable }   from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {isObject} from "rxjs/util/isObject";
import {isUndefined} from "util";

@Injectable()
export class ApiService  {
    private baseUrl = '/api/';

    constructor (private http: Http) {}

    private getBaseGetRequest(url, params = {}) {
        let options = new RequestOptions({params: params});
        return this.http.get(url, options)
            .map(this.extractData);
    }

    private getBasePostRequest(url, params = {}) {
        return this.http.post(url, params)
            .map(this.extractData);
    }

    private getUrl(methodName:string) {
        return this.baseUrl + methodName;
    }

    private extractData(res: Response) {
        if (res.status < 200 || res.status >= 300) {
            throw new Error('This request has failed ' + res.status);
        }

        let body = res.json();


        if (!isObject(body)) {
            throw new Error('This request is invalid ');
        }

        if (isUndefined(body.status) || body.status !== true) {
            throw body;
        }

        if (!isUndefined(body.data)) {
            return body.data;
        } else {
            return null;
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public getParsing(params) {
        let requestUrl = this.getUrl('getParsing');
        return this.getBasePostRequest(requestUrl, params);
    }

    public getList() {
        let requestUrl = this.getUrl('getList');
        return this.getBaseGetRequest(requestUrl);
    }

    public getParsed(id) {
        let requestUrl = this.getUrl('getParsed');
        return this.getBaseGetRequest(requestUrl, { id:id });
    }

}
