import {NgModule}               from '@angular/core';
import {BrowserModule}          from '@angular/platform-browser';
import {FormsModule}            from '@angular/forms';
import {ReactiveFormsModule}    from '@angular/forms';
import {HttpModule, JsonpModule} from '@angular/http';
import {requestOptionsProvider} from './default-request-options.service';
import {AppComponent}           from './app.component';
import {AppRoutingModule}       from './app-routing.module';
import {MainComponent}          from './main/main.component'
import {ListComponent}          from './list/list.component'
import {ParsedComponent}          from './parsed/parsed.component'

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        JsonpModule,
        AppRoutingModule,
        ReactiveFormsModule
    ],
    declarations: [
        AppComponent,
        MainComponent,
        ListComponent,
        ParsedComponent,
    ],
    providers: [requestOptionsProvider],
    bootstrap: [AppComponent]
})
export class AppModule {
}



