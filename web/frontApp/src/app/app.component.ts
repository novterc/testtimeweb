import {Component}                  from '@angular/core';
import {ApiService}                 from "./core/api.service";

@Component({
    selector: 'app-front',
    template: `<router-outlet></router-outlet>`,
    providers: [
        ApiService,
    ]
})
export class AppComponent {
    title = 'activity list';
}
