import {NgModule}                   from '@angular/core';
import {RouterModule, Routes}       from '@angular/router';
import {MainComponent}              from './main/main.component';
import {ListComponent}              from './list/list.component';
import {ParsedComponent}          from './parsed/parsed.component'

const routes: Routes = [
    {
        path: 'parsed/:parsedId',
        component: ParsedComponent
    },
    {
        path: 'list',
        component: ListComponent
    },
    {
        path: '',
        component: MainComponent
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {useHash: true})],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
