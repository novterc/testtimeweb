<?php

namespace src\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Parsed
 *
 * @ORM\Table(name="parsed")
 * @ORM\Entity
 */
class Parsed
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="page_url", type="string", length=255, nullable=true)
     */
    private $pageUrl;

    /**
     * @ORM\Column(name="math_count", type="integer", length=11, nullable=true)
     */
    private $mathsCount;

    /**
     * @ORM\Column(name="maths", type="json_array", nullable=true)
     */
    private $maths;

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set page url
     * @param string $pageUrl
     * @return Parsed
     */
    public function setPageUrl($pageUrl)
    {
        $this->pageUrl = $pageUrl;

        return $this;
    }

    /**
     * Get page url
     * @return string
     */
    public function getPageUrl()
    {
        return $this->pageUrl;
    }

    /**
     * @return mixed
     */
    public function getMathsCount()
    {
        return $this->mathsCount;
    }

    /**
     * @param mixed $mathsCount
     */
    public function setMathsCount($mathsCount)
    {
        $this->mathsCount = $mathsCount;
    }

    /**
     * @return mixed
     */
    public function getMaths()
    {
        return $this->maths;
    }

    /**
     * @param mixed $maths
     */
    public function setMaths($maths)
    {
        $this->maths = $maths;
    }

}
