<?php

namespace src\Controller;

use src\Factories\ParsingFactory;
use src\Entity\Parsed;
use src\Validators\ApiValidator;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends BaseController
{
    public function getParsingAction(Request $request)
    {
        try {
            $requestData = $this->getJsonData($request);
            $validate = ApiValidator::parsingRequest($requestData);
            if ($validate->valid === false) {
                return $this->getResponseForApiData([
                    'status' => true,
                    'data' => [
                        'validation' => $validate
                    ],
                ]);
            }

            $parsing = ParsingFactory::build($requestData->type, $requestData->url, $requestData->text ?? null);
            $parsing->run();

            if($parsing->isError) {
                return $this->getResponseForApiData([
                    'status' => false,
                    'reason' => 'error parsing',
                ]);
            }

            $parsed = new Parsed();
            $parsed->setPageUrl($requestData->url);
            $parsed->setMathsCount($parsing->mathsCount);
            $parsed->setMaths($parsing->maths);
            $this->em->persist($parsed);
            $this->em->flush();

            return $this->getResponseForApiData([
                'status' => true,
                'data' => [
                    'validation' => $validate,
                    'mathsCount' => $parsing->mathsCount,
                    'parsedId' => $parsed->getId(),
                ]
            ]);

        } catch ( \Exception $exception) {
            return $this->getResponseForApiData([
                'status' => false,
            ]);
        }
    }

    public function getListAction()
    {
        $parsedRepository = $this->em->getRepository(Parsed::class);
        $parsed = $parsedRepository->findAll();
        $parsedData = array_map(function($item){
            return $this->parsedDto($item);
        }, $parsed);

        return $this->getResponseForApiData([
            'status' => true,
            'data' => $parsedData
        ]);
    }

    public function getParsedAction(Request $request)
    {
        $parsedRepository = $this->em->getRepository(Parsed::class);
        $parsed = $parsedRepository->find((int) $request->get('id'));
        if(!$parsed) {
            return $this->getResponseForApiData([
                'status' => false,
                'reason' => 'not found',
            ]);
        }

        return $this->getResponseForApiData([
            'status' => true,
            'data' => $this->parsedDto($parsed, true),
        ]);
    }

    protected function parsedDto(Parsed $item, $withMaths = false)
    {
        //TODO I know this should not exist
        $resultObj = new \stdClass();
        $resultObj->id = $item->getId();
        $resultObj->pageUrl = $item->getPageUrl();
        $resultObj->mathsCount = $item->getMathsCount();
        if($withMaths){
            $resultObj->maths = $item->getMaths();
        }

        return $resultObj;
    }
}
