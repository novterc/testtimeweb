<?php

namespace src\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;

class MainController extends BaseController
{
    public function indexAction()
    {
        return $this->render('pages/main_index.html.twig');
    }

    public function installAction()
    {
        $query = file_get_contents(__DIR__ . '/../../resources/dump.sql');
        $stmt = $this->em->getConnection()->prepare($query);
        $stmt->execute();

        return new RedirectResponse('/');
    }
}
