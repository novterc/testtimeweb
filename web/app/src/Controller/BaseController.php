<?php

namespace src\Controller;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Twig_Environment;

class BaseController
{
    protected $em;
    protected $twig;

    /**
     * BannerController constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, Twig_Environment $twig)
    {
        $this->em = $em;
        $this->twig = $twig;
    }

    /**
     * Get Response instance for upload file by path
     * @param string $filePath
     * @return Response
     */
    protected function getResponseForFileUpload(string $filePath)
    {
        $response = new Response();
        $response->headers->set('Content-type', mime_content_type($filePath));
        $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($filePath) . '";');
        $response->headers->set('Content-length', filesize($filePath));
        $response->headers->set('Content-Transfer-Encoding', "binary");

        return $response->setContent(file_get_contents($filePath));
    }

    protected function getResponseForApiData($data)
    {
        $response = new JsonResponse();
        $response->setContent(json_encode($data));

        return $response;
    }

    /**
     * Renders a view.
     *
     * @param string   $view       The view name
     * @param array    $parameters An array of parameters to pass to the view
     * @param Response $response   A response instance
     *
     * @return Response A Response instance
     */
    protected function render($view, array $parameters = array(), Response $response = null)
    {
        if (null === $response) {
            $response = new Response();
        }


        $response->setContent($this->twig->render($view, $parameters));

        return $response;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getJsonData(Request $request)
    {
        return json_decode($request->getContent());
    }
}
