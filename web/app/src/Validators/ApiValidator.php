<?php

namespace src\Validators;

use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Url;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validation;

class ApiValidator
{
    /**
     * Validate request for parsing
     * @param \stdClass $requestData
     * @return \stdClass
     */
    public static function parsingRequest(\stdClass $requestData)
    {
        $returnObj = new \stdClass();
        $returnObj->valid = true;
        $returnObj->errors = [];

        $validator = Validation::createValidator();


        $violations = $validator->validate($requestData->url, [
            new Url(),
        ]);
        static::handlerApiValidate($violations, $returnObj, 'url');


        $violations = $validator->validate($requestData->type, [
            new Choice(['image', 'link', 'text']),
        ]);
        static::handlerApiValidate($violations, $returnObj, 'type');


        if($requestData->type === 'text') {
            $violations = $validator->validate($requestData->text, [
                new Length([ 'min' => 1, 'max' => 255]),
            ]);
            static::handlerApiValidate($violations, $returnObj, 'text');
        }


        return $returnObj;
    }

    /**
     * @param ConstraintViolationListInterface $violations
     * @param \stdClass $returnObj
     * @param string $fieldName
     */
    protected static function handlerApiValidate(ConstraintViolationListInterface $violations, \stdClass &$returnObj, string $fieldName):void
    {
        if($violations->count() === 0) {
            return;
        }

        $returnObj->valid = false;
        $errors = [];
        /** @var ConstraintViolation $violation */
        foreach ($violations->getIterator() as $violation) {
            $errors[] = $violation->getMessage();
        }
        $returnObj->errors[$fieldName] = $errors;
    }
}