<?php

namespace src;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use src\Controller\BannerController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Twig_Environment;
use Twig_Loader_Filesystem;

class AppKernel
{
    protected $config;
    protected $isDevMode;
    protected $entityManager;
    protected $twig;

    /**
     * AppKernel constructor.
     * @param array
     * @param boolean $devMode
     */
    public function __construct(array $config, bool $isDevMode = false)
    {
        $this->config = $config;
        $this->isDevMode = $isDevMode;
    }

    /**
     * Get config data by path
     * @param string $path
     * @return array|mixed
     * @throws \Exception
     */
    public function getConfig($path = '')
    {
        $keyArr = explode('.', $path);
        $config = $this->config;
        if (!empty($keyArr)) {
            foreach ($keyArr as $key) {
                if (!isset($key)) {
                    throw new \Exception('not found config path:' . $path);
                }
                $config = $config[$key];
            }
        }

        return $config;
    }

    /**
     * Get mode status
     * @return bool
     */
    public function isDevMode()
    {
        return $this->isDevMode;
    }

    /**
     * Get entity manager instance
     * @return EntityManager
     */
    public function getEntityManager()
    {
        if ($this->entityManager === null) {
            $config = Setup::createAnnotationMetadataConfiguration($this->getConfig('paths.entity'), $this->isDevMode(), null, null, false);
            $entityManager = EntityManager::create($this->getConfig('dbParams'), $config);
            $this->entityManager = $entityManager;
        }

        return $this->entityManager;
    }

    /**
     * Get twig instance
     * @return Twig_Environment
     */
    public function getTwig()
    {
        if(empty($this->twig)) {
            $loader = new Twig_Loader_Filesystem($this->getConfig('paths.templates'));
            $twig = new Twig_Environment($loader);
            $this->twig = $twig;
        }

        return $this->twig;
    }

    /**
     * Handle http request instance
     * @param Request $request
     * @return Response
     */
    public function handlerHtppRequest(Request $request)
    {
        try {
            $path = $this->getConfig('paths.routes');
            $locator = new FileLocator($path);
            $loader = new YamlFileLoader($locator);
            $mainRouteYml = $this->getConfig('paths.route.main');
            $collection = $loader->load($mainRouteYml);

            $context = new RequestContext();
            $context->fromRequest($request);
            $matcher = new UrlMatcher($collection, $context);

            $attributes = $matcher->match($request->getPathInfo());
            $controller = $attributes['_controller'] ?? null;
            if(empty($controller)) {
                throw new Exception('controller is empty');
            }

            list($controllerObj, $method) = $this->createController($controller);
            $response = $controllerObj->{$method}($request);
            if(!is_a($response, Response::class)) {
                throw new \Exception('the controller must return a "Response" instance');
            }
        } catch (\Exception $exception) {
            $response = new Response();
            $response->setStatusCode(500);
            if ($this->isDevMode()) {
                throw $exception; //TODO uncomment only for debug
//                $response->setContent($exception->getMessage() . '-' . $exception->getFile() . ':' . $exception->getLine());
            }
        }

        return $response;
    }

    /**
     * Returns a callable for the given controller.
     *
     * @param string $controller A Controller string
     *
     * @return callable A PHP callable
     *
     * @throws \InvalidArgumentException
     */
    protected function createController($controller)
    {
        if (false === strpos($controller, '::')) {
            throw new \InvalidArgumentException(sprintf('Unable to find controller "%s".', $controller));
        }

        list($class, $method) = explode('::', $controller, 2);

        if (!class_exists($class)) {
            throw new \InvalidArgumentException(sprintf('Class "%s" does not exist.', $class));
        }

        return array($this->instantiateController($class), $method);
    }

    /**
     * Returns an instantiated controller.
     *
     * @param string $class A class name
     *
     * @return object
     */
    protected function instantiateController($class)
    {
        return new $class($this->getEntityManager(), $this->getTwig());
    }

}
