<?php

namespace src\Factories;

class ParsingLinkFactoryItem extends ParsingFactoryItem
{
    function responsePregMath(string $content)
    {
        preg_match_all(
            '/<a\s[^>]*href=\"([^\"]*)\"[^>]*>(.*)<\/a>/siU',
            $content,
            $maths,
            PREG_PATTERN_ORDER
        );

        return $maths[1] ?? null;
    }
}
