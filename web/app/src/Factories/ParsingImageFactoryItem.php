<?php

namespace src\Factories;

class ParsingImageFactoryItem extends ParsingFactoryItem
{
    function responsePregMath(string $content)
    {
        preg_match_all(
            '/< *img[^>]*src *= *["\']?([^"\']*)/i',
            $content,
            $maths,
            PREG_PATTERN_ORDER
        );

        return $maths[1];
    }
}
