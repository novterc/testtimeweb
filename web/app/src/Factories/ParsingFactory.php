<?php

namespace src\Factories;

use Symfony\Component\Config\Definition\Exception\Exception;

class ParsingFactory
{
    /**
     * @param string $type
     * @param string $url
     * @param string|null $text
     * @return ParsingFactoryItem
     */
    public static function build(string $type, string $url, string $text = null)
    {
        $className = 'src\Factories\Parsing' . ucwords($type) . 'FactoryItem';
        if(!class_exists($className)) {
            throw new Exception('this type not supporting :' . $className);
        }
        $classInstance = new $className($url, $text);

        return $classInstance;
    }
}