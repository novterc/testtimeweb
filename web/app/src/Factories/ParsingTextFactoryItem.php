<?php

namespace src\Factories;

class ParsingTextFactoryItem extends ParsingFactoryItem
{
    protected $text;

    public function __construct(string $url, string $text)
    {
        parent::__construct($url);
        $this->text = $text;
    }

    function responsePregMath(string $content)
    {
        preg_match_all(
            '/'. preg_quote($this->text, '/') .'/i',
            $content,
            $maths,
            PREG_PATTERN_ORDER
        );

        return $maths[0] ?? null;
    }


}