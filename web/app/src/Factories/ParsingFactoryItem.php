<?php

namespace src\Factories;

abstract class ParsingFactoryItem
{
    protected $url;
    public $curl;
    public $isError = false;
    public $maths = [];
    public $mathsCount = 0;

    public function __construct(string $url)
    {
        $this->url = $url;
    }

    public function run()
    {
        $response = $this->getCurlResponse();
        if($response === null) {
            return;
        }

        $this->maths = $this->responsePregMath($response);
        $this->mathsCount = count($this->maths);
    }

    /**
     * @return null|string
     */
    public function getCurlResponse()
    {
        $curl = new \Curl\Curl();
        $this->curl = $curl;
        $curl->get($this->url);
        if ($curl->error) {
            $this->setError();

            return null;
        }

        return $curl->response;
    }

    abstract function responsePregMath(string $content);

    protected function setError()
    {
        $this->isError = true;
    }

}