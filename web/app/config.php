<?php

return [

    'paths' => [
        'entity' => ['src/Entity'],
        'routes' => __DIR__ . '/resources/routes',
        'route' => ['main' => 'main.yml'],
        'templates' => __DIR__ . '/resources/views',
    ],

    'dbParams' => [
        'driver' => 'mysqli',
        'host' => 'mysql',
        'dbname' => 'test',
        'user' => 'root',
        'password' => 'root',
    ],

];
