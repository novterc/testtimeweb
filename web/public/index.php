<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

use src\AppKernel;
use Symfony\Component\HttpFoundation\Request;

require_once __DIR__ . '/../app/bootstrap.php';

$config = include __DIR__ . '/../app/config.php';
$kernel = new AppKernel($config, true);

$request = Request::createFromGlobals();
$response = $kernel->handlerHtppRequest($request);
$response->send();
